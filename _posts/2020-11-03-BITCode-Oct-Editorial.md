---
layout: post
title: BITCode October Round Editorial
subtitle: Editorial
# bigimg: 
#   - "https://cdn.pixabay.com/photo/2016/11/29/05/55/asphalt-1867667_960_720.jpg" : "Contest"

css: /assets/css/styles.css
author: Anjani Kumar

tags : [BITCode October, solutions, editorial, competitive programming]
---

Hi. Thank you for participating in the BITCode October Round. We hope that you enjoyed the problems we prepared for you. We would like to apologize for the late editorial. This time we reduced the difficulty. Incase, if you weren't
able to solve them, here are the solutions with tested code that you can read to figure out what went wrong in the contest.
Upsolving the problems is the most important thing to do after a contest. Here they are:-

* [Chef and His Homework](https://discuss.codechef.com/t/chefhw-editorial/80634)
* [Chef and Sticks](https://discuss.codechef.com/t/chfstik-editorial/80493)
* [Adventures Of Rick I](https://discuss.codechef.com/t/rick1-editorial/80635)
* [Agent Binod](https://discuss.codechef.com/t/editorial-atbin/80492)
* [Last Non-Zero Digit](https://discuss.codechef.com/t/factdig-editorial/80491)

Thank you
Happy Learning!!!