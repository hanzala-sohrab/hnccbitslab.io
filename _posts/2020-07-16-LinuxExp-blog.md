---
layout: post
title: Linux Experience !
subtitle:  My first step to Linux(Ubuntu).
image: https://png.pngitem.com/pimgs/s/221-2211837_transparent-linux-penguin-png-ubuntu-linux-logo-png.png
css: /assets/css/styles.css
bigimg: 
  - "https://live.staticflickr.com/4082/4748828036_e8bf774155_b.jpg" : "Ubuntu Experience."

author: Zeeshan Ashraf - ECE 2k19

tags : [intro, experience, linux,ubuntu, 2020]
---

**Hello! Linux enthusiast, I am Zeeshan, a member of HnCC BIT. Well, with this blog I wanna share my initial struggle and experience exploring Linux.**.

I came to know about the Linux operating system when I was in the 10th standard. Its features like it are highly compatible, almost virus-free, fast, highly customizable and we can have access to its source code. Though I didn’t use it early because I guess I was busy exploring GUI of Windows and yes, was having *Linux-Fear*. Yes, **Linux-Fear** which I guess some of the newbies like me have, like *how to install?*, *Is it difficult to use?*.

```bash
 sudo -h
```

*****

Now, as I am in college I got to learn a lot about Linux and its properties. I installed **Ubuntu**, a Linux distribution based on Debian with GNOME user interface. Initially, during installation, I faced a bit of a problem while installing it. The problem I faced was due to the NVIDIA graphics card driver because of which my installation got several times. Finally, my seniors suggested me to install in safe graphics mode and also change certain settings before proceeding. After installation, I faced a few more challenges and of them was wifi-driver and ethernet card not functioning. But lastly, with the help of seniors and friends, everything got installed properly.

```bash
sudo apt-get update
```

*****

Initially, I find it a bit uncomfortable but within a few days, things become a lot comfortable and interesting. Ubuntu has great features. We can update the entire system while working on it(most of the time). Ubuntu has a great open-source software library for almost every field of work like LibreOffice, Inkspace, VLC media player, Stacer, KODI, etc. The best thing that I found about the Linux operating system is that it encourages Command Based User Interface giving us a good habit of using the command line instead of unnecessary clicks. Since I am interested in web development where I have to use command prompt and line, Linux helps me in developing a habit of using the command line. 

### My Setup

![setup](https://hnccbits.github.io/cdn/images/blog/2020/linuxexp1.jpeg)

Ubuntu or say Linux has great online community support for almost every issue and answer for every query which helps to get comfortable with Ubuntu a lot easier. Customizability and complete control over our entire system create a huge curiosity for learning and understanding. Though I find Ubuntu bit slow while booting otherwise it works fine while working on it and can completely control every process with the terminal. It doesn’t have a great GUI like Windows but who needs it when we have codes. I still have to explore more as I am just a beginner to Linux but as per now, I find Linux bit complicated(maybe to its different commands) but exciting!

```bash
sudo shutdown now
```

## Use Linux, Feel Freedom, Be Cool and Just sudo it
